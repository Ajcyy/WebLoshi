package reone.loushi.web;

import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.Fail;
import org.nutz.mvc.annotation.IocBy;
import org.nutz.mvc.annotation.Modules;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.SetupBy;
import org.nutz.mvc.ioc.provider.ComboIocProvider;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

@SetupBy(value=MainSetup.class)
@IocBy(type=ComboIocProvider.class, args={"*js", "conf/ioc/",
        "*anno", "reone.loushi.web",
        "*tx", // 事务拦截 aop
        "*async"}) // 异步执行aop
@Modules(scanPackage=true)
@Ok("raw")
@Fail("http:500")
public class MainModule {

	@At("/")
	public String index(){
		return "Welcome to Loushi!!";
	}

	@At
	public JSON json(){
		JSONObject demo = new JSONObject();
		demo.put("demokey", "Hello word");
		return demo;
	}
	
}
