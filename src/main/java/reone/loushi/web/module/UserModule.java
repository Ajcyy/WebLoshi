package reone.loushi.web.module;

import java.util.UUID;

import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.Fail;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.Param;

import reone.loushi.web.bean.ErrorCode;
import reone.loushi.web.bean.Result;
import reone.loushi.web.bean.User;
import reone.loushi.web.service.UserService;

@IocBean
@At("/user")
@Ok("json")
@Fail("http:500")
public class UserModule {
	
	@Inject("userService")
	private UserService userService;
	
	/**
	 * 注册
	 * @return
	 */
	@At
	public Result regist(@Param("number") String number,@Param("passwd") String passwd){
		User user = userService.checkUserByNumber(number);
		if(user==null){
			user = new User();
			user.setUuid(UUID.randomUUID().toString());
			user.setNumber(number);
			user.setPassword(passwd);
			if(userService.saveUser(user)){
				return Result.doSuccess("regist success", user);
			}else{
				return Result.doException("regist saveUser exception");
			}
		}else{
			return Result.doError(ErrorCode.USER_ALREADY_EXIST);
		}
	}
	
	/**
	 * 登录
	 * @param passwd
	 * @return
	 */
	@At
	public Result login(@Param("number") String number,@Param("passwd") String passwd){
		User user = userService.checkUserByNumber(number);
		if(user!=null){
			if(user.getPassword().equals(passwd)){
				return Result.doSuccess("regist success", user.getUuid());
			}
		}
		return Result.doError(ErrorCode.USER_LOGIN_FAIL);
	}
	@At 
	public Result logout(){
		return Result.doSuccess("logout success");
	}

}
