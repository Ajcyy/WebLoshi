package reone.loushi.web.bean;

/**
 * 错误码
 * 
 * 基本 0+
 * 账号 100+
 * 权限 200+
 * 社交 300+
 * 
 * @author reone
 *
 */
public class ErrorCode {
	
	/**
	 * 正常错误码
	 */
	public static final int SUCCESS = 0;
	
	/**
	 * 普通异常
	 */
	public static final int EXCEPTION = 1;
	
	/**
	 * 注册用户已经存在
	 */
	public static final int USER_ALREADY_EXIST = 101;
	
	/**
	 * 用户未登录
	 */
	public static final int USER_NOT_LOGIN = 102;
	
	/**
	 * 登录失败,用户或秘密错误
	 */
	public static final int USER_LOGIN_FAIL = 103;
	
	/**
	 * 权限不够
	 */
	public static final int PERMISSION_LOW = 204;
}
