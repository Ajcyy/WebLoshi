package reone.loushi.web.bean;

import java.io.Serializable;

import org.nutz.dao.entity.annotation.ColDefine;
import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.Index;
import org.nutz.ioc.loader.annotation.IocBean;

@IocBean
@Index(fields = { "number" }, name = "index_number")
public class User implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7888444539933398970L;
	/**
	 * 数据库唯一标识
	 */
	@Column
	@ColDefine(width = 32, notNull = true)
	private String uuid;
	
	/**
	 * 用户电话号码，用户登录用的唯一标识
	 */
	@Column
	private String number;
	
	/**
	 * 用户昵称
	 */
	@Column
	private String name;
	/**
	 * 用户密码
	 */
	@Column
	@ColDefine(width = 32, notNull = true)
	private String password;
	/**
	 * 用户年龄
	 */
	@Column
	private int age;
	/**
	 * 用户性别
	 */
	@Column
	private String male;
	/**
	 * 用户头像信息
	 */
	@Column
	private String headUrl;
	/**
	 * 用户称号 flag
	 */
	@Column
	private String flag;

	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getMale() {
		return male;
	}
	public void setMale(String male) {
		this.male = male;
	}
	public String getHeadUrl() {
		return headUrl;
	}
	public void setHeadUrl(String headUrl) {
		this.headUrl = headUrl;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
}
