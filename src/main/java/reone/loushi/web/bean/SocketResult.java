package reone.loushi.web.bean;

import java.io.Serializable;

import com.alibaba.fastjson.JSON;


public class SocketResult implements Serializable{
	
	private static final long serialVersionUID = 3796406908917209339L;

	public class Event{
		public static final String MESSAGE = "message";
		public static final String WARN = "warn";
		public static final String ERROR = "error";
		public static final String NOTIFY = "notify";
	}
	private String event;
	private String message;
	
	public SocketResult(String event,String message){
		this.event = event;
		this.message = message;
	}
	
	@Override
	public String toString() {
		return JSON.toJSONString(this);
	}
	
	public String getEvent() {
		return event;
	}
	public void setEvent(String event) {
		this.event = event;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
}
