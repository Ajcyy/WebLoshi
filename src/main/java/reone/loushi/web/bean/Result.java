package reone.loushi.web.bean;

import java.io.Serializable;

public class Result implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6140681595419052748L;

	/**
	 * 表示请求的处理状态
	 * <p> 值为"success":请求处理成功
	 * <p> 值为"error":请求失败
	 * <p> 值为"exception":请求过程中发生异常
	 */
	private int errorCode;
	
	//请求结果的描述
	private String message;
	
	//请求的具体数据
	private Object data;

	public Result(){}
	
	public Result(int errorCode, String message, Object data) {
		super();
		this.errorCode = errorCode;
		this.message = message;
		this.data = data;
	}
	public static Result doSuccess(String string) {
		return new Result(ErrorCode.SUCCESS,null,null);
	}
	
	public static Result doSuccess(String message, Object data) {
		return new Result(ErrorCode.SUCCESS,message,data);
	}

	public static Result doException(String message) {
		return new Result(ErrorCode.EXCEPTION,message,null);
	}

	public static Result doError(int errorCode) {
		return new Result(errorCode,null,null);
	}
	
	
	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

}
