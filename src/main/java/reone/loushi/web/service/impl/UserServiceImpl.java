package reone.loushi.web.service.impl;

import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.service.NameEntityService;

import reone.loushi.web.bean.User;
import reone.loushi.web.service.UserService;


@IocBean(args="userService")
public class UserServiceImpl extends NameEntityService<User> implements UserService{

	@Override
	public User checkUserById(String id) {
		return null;
	}

	@Override
	public boolean saveUser(User user) {
		return false;
	}

	@Override
	public boolean removeUserById(String id) {
		return false;
	}

	@Override
	public boolean UpdateUser(User newUser) {
		return false;
	}

	@Override
	public User checkUserByNumber(String number) {
		return null;
	}
	
}
