package reone.loushi.web.service;

import reone.loushi.web.bean.User;

public interface UserService {

	public User checkUserByNumber(String number);
	
	public User checkUserById(String id);
	
	public boolean saveUser(User user);
	
	public boolean removeUserById(String id);
	
	public boolean UpdateUser(User newUser);
}
