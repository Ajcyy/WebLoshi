package reone.loushi.web.util;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

/**
 * 读取properties文件，get取值
 * @author reone
 *
 */
public class PropertiesUtil {
	private static final String PROJECT_PROPERTIES_PATH= "/project.properies";
    private static Properties prop;
    
    public static PropertiesUtil getProjectProperties(){
    	return new PropertiesUtil(PROJECT_PROPERTIES_PATH);
    }  
    
    public PropertiesUtil(String properties_path) {
    	prop =  new  Properties();    
    	InputStream in = PropertiesUtil.class.getResourceAsStream(properties_path);    
        try {
            prop.load(in);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 通过key获取path = /ex.properties的value
     * @param key
     * @param defValue 默认值
     * @return string value
     */
    public String get(String key,String defValue){
        return prop.getProperty(key, defValue);
    }
    /**
     * 通过key获取path = /ex.properties的value
     * @param key
     * @return string value
     */
    public String get(String key){
        return prop.getProperty(key);
    }

    /**
     * 通过Key获取value，并转化成set，分隔符 ','
     * @param key
     * @return Set
     */
    public Set<String> getSet(String key){
        Set<String> set = new HashSet<>();
        String setString = get(key);
        String[] values = setString.split(",");
        set.addAll(Arrays.asList(values));
        return set;
    }

    public Properties getProperties(){
        return prop;
    }
}